package fr.univlille.iutinfo.r304.thermo;

import fr.univlille.iutinfo.r304.thermo.part1.model.Thermogeekostat;
import fr.univlille.iutinfo.r304.thermo.part1.view.TextView;
import javafx.application.Application;
import javafx.stage.Stage;

public class Main extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		Thermogeekostat thermo = new Thermogeekostat();
		new TextView(thermo);
	}

}
