package fr.univlille.iutinfo.r304.thermo.part1.model;

import fr.univlille.iutinfo.r304.utils.Subject;

public class Thermogeekostat extends Subject implements ITemperature {

	private double temperature = 0;

	@Override
	public void setTemperature(double d) {
		this.temperature = d;

	}

	@Override
	public Double getTemperature() {

		return this.temperature;
	}

	@Override
	public void incrementTemperature() {
		setTemperature(getTemperature() + 1);

	}

	@Override
	public void decrementTemperature() {
		setTemperature(getTemperature() - 1);

	}

}
