package fr.univlille.iutinfo.r304.thermo.part1.view;

import fr.univlille.iutinfo.r304.thermo.part1.model.Thermogeekostat;
import fr.univlille.iutinfo.r304.utils.Observer;
import fr.univlille.iutinfo.r304.utils.Subject;
import javafx.stage.Stage;

public class TextView extends Stage implements ITemperatureView, Observer {

	Thermogeekostat model;

	public TextView(Thermogeekostat model) {
		this.model = model;
	}

	@Override
	public double getDisplayedValue() {
		return model.getTemperature();
	}

	@Override
	public void incrementAction() {
		// TODO Auto-generated method stub

	}

	@Override
	public void decrementAction() {
		// TODO Auto-generated method stub

	}

	@Override
	public void update(Subject subj) {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException("Unimplemented method 'update'");
	}

	@Override
	public void update(Subject subj, Object data) {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException("Unimplemented method 'update'");
	}

}
